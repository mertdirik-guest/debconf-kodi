TARGET_DIR = .

VERSION=$(shell grep "addon id" addon.xml |cut -d '"' -f 6)

zip:
	git archive --prefix=plugin.video.debconf/ HEAD --format=zip -o $(TARGET_DIR)/plugin.video.debconf-$(VERSION).zip
